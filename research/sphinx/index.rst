.. CICD project illustration documentation master file, created by
   sphinx-quickstart on Sun Apr 21 20:06:36 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CICD project illustration's documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   research.ipynb
   helper


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
