import plotly.express as px
import pandas as pd

def missing_values_share(dataset):
    for col in dataset.columns:
        print(f'Column: {col} contains {dataset[col].isna().sum()/dataset.shape[0]*100:.2f} % missing values')


def unique_values_number(dataset):
    for col in dataset.columns:
        print(f'Column: {col} contains {dataset[col].nunique()} distinct values')

def plot_geographical_distribution(data):
    data['lat_rounded'] = data['latitude'].round(2)  
    data['lon_rounded'] = data['longitude'].round(2)
    aggregated_data = data.groupby(['lat_rounded', 'lon_rounded']).size().reset_index(name='counts')

    fig = px.scatter_geo(aggregated_data, lat='lat_rounded', lon='lon_rounded',  projection="natural earth")
    fig.update_geos(
        center=dict(lat=aggregated_data['lat_rounded'].mean(), lon=aggregated_data['lon_rounded'].mean()),
        lataxis_range=[aggregated_data['lat_rounded'].min(), aggregated_data['lat_rounded'].max()], 
        lonaxis_range=[aggregated_data['lon_rounded'].min(), aggregated_data['lon_rounded'].max()]  
    )

    fig.update_layout(
        geo=dict(
            resolution=50,  
            showland=True,  
            landcolor="lightgreen",
            showocean=True,  
            oceancolor="lightblue",
            showcountries=True,  
        ),
        title=dict(x=0.5)  
    )


    fig.show()