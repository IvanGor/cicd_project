FROM python:3.9-slim
WORKDIR /app
# Install git and pdm
RUN apt-get update && apt-get install -y git && apt-get install -y unzip && pip install pdm && pip install kaggle \
    && apt-get install -y pandoc && apt-get install -y gcc && apt-get install -y make
COPY .pre-commit-config.yaml pyproject.toml pdm.lock* /app/
RUN git init \
    && git config user.name "Your name" \
    && git config user.email "your@email.com" \
    && git add . \
    && git commit -m "Initial commit"
RUN pdm install

RUN pdm add --dev pre-commit
RUN pdm add --dev ruff
RUN pdm run pre-commit install
RUN mkdir data && cd data && kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data \
    && unzip ny-2015-street-tree-census-tree-data.zip \
    && rm ny-2015-street-tree-census-tree-data.zip \
    && ls
    
RUN pip install sphinx nbsphinx pandoc 
RUN pip install --upgrade sphinx_rtd_theme
COPY research /app/research
RUN chown -R nobody:nogroup /app/research
RUN pip install -r research/requirements.txt
RUN pip install nbconvert jupyter
RUN cd research/sphinx && jupyter nbconvert --to notebook --execute --inplace research.ipynb
RUN cd research/sphinx && make html
RUN rm -rf data




